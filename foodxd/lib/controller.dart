import 'package:flutter/material.dart';
import 'screens/googleMap.dart';
import 'screens/foodListing.dart';
import 'screens/resListing.dart';
import 'main.dart';
import 'screens/user/userFav.dart';
import 'screens/user/userFriend.dart';
import 'screens/user/userSettings.dart';

void onTapFoodPage(context) {
  Navigator.of(context)
      .push(MaterialPageRoute(builder: (context) => FoodListing()));
}

void onTapResPage(context) {
  Navigator.of(context)
      .push(MaterialPageRoute(builder: (context) => ResListing()));
}

void onTapFav(context) {
  Navigator.of(context)
      .push(MaterialPageRoute(builder: (context) => UserFavRes()));
}

void onTapFriend(context) {
  Navigator.of(context)
      .push(MaterialPageRoute(builder: (context) => UserFriend()));
}

void onTapSettings(context) {
  Navigator.of(context)
      .push(MaterialPageRoute(builder: (context) => UserSettings()));
}

void onLocationTap(BuildContext context, int resID) {
  Navigator.pushNamed(context, ResPage, arguments: {"id": resID});
}

void onFoodLocationTap(BuildContext context, String keyword) {
  Navigator.pushNamed(context, KeywordResPage, arguments: {"keyword": keyword});
}

void onTapGoogleMaps(context) {
  Navigator.of(context).push(MaterialPageRoute(builder: (context) => MyApp()));
}
