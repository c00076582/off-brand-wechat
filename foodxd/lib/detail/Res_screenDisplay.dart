import 'package:flutter/material.dart';

class Res_detail extends StatelessWidget {
  final String _title;
  final String _body;
  Res_detail(this._title, this._body);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: const EdgeInsets.fromLTRB(16.0, 18.0, 16, 6.0),
            child: Text(_title, style: Theme.of(context).textTheme.title),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(16.0, 6.0, 16, 10.0),
            child: Text(_body, style: Theme.of(context).textTheme.body1),
          ),
        ]);
  }
}
