class Restaurant {
  final int id;
  final String name;
  final String imgPath;
  final List<Res_info> info;
  final String review;
  final String priceLevel;
  final double lat;
  final double lng;
  final String keyword;

  Restaurant(
    this.id,
    this.name,
    this.imgPath,
    this.review,
    this.priceLevel,
    this.lat,
    this.lng,
    this.keyword,
    this.info,
  );

  //temp constructor, testing purpose
  static List<Restaurant> fetchAll() {
    return [
      Restaurant(
        1,
        "Bisbano's Pizza",
        "assets/images/bisbanoPizzaParlor.jpg",
        "4.5",
        "1",
        30.2136768,
        -92.02301129999999,
        "Pizza",
        [
          Res_info("Restaurant Address", "1540 Johnston St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        2,
        "Tsunami Sushi",
        "assets/images/tsunami.jpg",
        "4.6",
        "3",
        30.2264098,
        -92.01791559999999,
        "Sushi",
        [
          Res_info("Restaurant Address", "412 Jefferson St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        3,
        "CENTRAL Pizza & Bar",
        "assets/images/centralPizza.jpg",
        "4.6",
        "2",
        30.2259532,
        -92.0177442,
        "Pizza",
        [
          Res_info("Restaurant Address", "423 Jefferson St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        4,
        "Ground Pat'i",
        "assets/images/groundPat.png",
        "4.2",
        "N/A",
        30.210169,
        -92.0295,
        "Burger",
        [
          Res_info("Restaurant Address", "2303 Johnston St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        5,
        "Pizza Hut",
        "assets/images/pizzahut.jpg",
        "3.9",
        "1",
        30.2199616,
        -92.023724,
        "Pizza",
        [
          Res_info("Restaurant Address", "105 St Landry St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        6,
        "Gary's",
        "assets/images/garyBuilding.jpg",
        "4.7",
        "1",
        30.222026,
        -92.01222700000001,
        "Burger",
        [
          Res_info("Restaurant Address", "104 Lamar St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        7,
        "Burger King",
        "assets/images/burgerKing.jpg",
        "3.4",
        "1",
        30.21579357989272,
        -92.02082062010726,
        "Burger",
        [
          Res_info("Restaurant Address", "1500 Johnston St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        8,
        "La Carreta",
        "assets/images/lacarreta.jpg",
        "4.5",
        "1",
        30.226645,
        -92.01767819999999,
        "Taco",
        [
          Res_info("Restaurant Address", "400 Johnston St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        9,
        "Taco Sisters",
        "assets/images/tacosista.jpg",
        "4.5",
        "1",
        30.2232337,
        -92.01494369999999,
        "Taco",
        [
          Res_info("Restaurant Address", "407 Johnston St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        10,
        "Reggie's SoulFood",
        "assets/images/reggi.jpg",
        "4.6",
        "1",
        30.2283762,
        -92.01850139999999,
        "N/A",
        [
          Res_info("Restaurant Address", "519 S Pierce St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        11,
        "Papa John's Pizza",
        "assets/images/papaJohn.jpg",
        "3.6",
        "1",
        30.2152492,
        -92.0215494,
        "Pizza",
        [
          Res_info("Restaurant Address", "1418 Johnston St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
      Restaurant(
        12,
        "Izzo's Illegal Burrito",
        "assets/images/izzo.jpg",
        "4.4",
        "1",
        30.211357,
        -92.02860769999999,
        "Taco",
        [
          Res_info("Restaurant Address", "2010 Johnston St, Lafayette"),
          Res_info("Time", "8:00-5:00"),
          Res_info("Restaurant Info",
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        ],
      ),
    ];
  }

  //this is temp, replace by google maps API to get the restaurant.
  //for testing purpose only
  static Restaurant fetchByID(int resID) {
    List<Restaurant> res = Restaurant.fetchAll();
    for (var i = 0; i < res.length; i++) {
      if (res[i].id == resID) {
        return res[i];
      }
    }
    return null;
  }

  static Restaurant fetchByKeyword(String keyword) {
    List<Restaurant> res = Restaurant.fetchAll();
    for (var i = 0; i < res.length; i++) {
      if (res[i].keyword == keyword) {
        return res[i];
      }
    }
    return null;
  }
}

class Res_info {
  final String title;
  final String text;
  Res_info(this.title, this.text);
}
