class Res_info {
  final String title;
  final String text;
  Res_info(this.title, this.text);
}

class Food {
  final int id;
  final String name;
  final String imgPath;
  final String keyword;

  Food(
    this.id,
    this.name,
    this.imgPath,
    this.keyword,
  );

  //temp constructor, testing purpose
  static List<Food> fetchAll() {
    return [
      Food(
        1,
        "Burger",
        "assets/images/burger.jpg",
        "burger",
      ),
      Food(
        2,
        "Taco",
        "assets/images/taco.png",
        "taco",
      ),
      Food(
        3,
        "Pizza",
        "assets/images/pizza.jpg",
        "pizza",
      ),
      Food(
        4,
        "Sushi",
        "assets/images/sushi.jpg",
        "sushi",
      )
    ];
  }

  //this is temp, replace by google maps API to get the restaurant.
  //for testing purpose only
  static Food fetchByID(String name) {
    List<Food> food = Food.fetchAll();
    for (var i = 0; i < food.length; i++) {
      if (food[i].name == name) {
        return food[i];
      }
    }
    return null;
  }
}
