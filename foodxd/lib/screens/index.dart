import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fooxd/controller.dart';

class IndexPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("FoodXD"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.favorite),
              onPressed: () {
                onTapFav(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.person_add),
              onPressed: () {
                onTapFriend(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                onTapSettings(context);
              },
            ),
          ],
        ),
        body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              "What Do You Want?",
              style: TextStyle(
                fontSize: 36,
                fontWeight: FontWeight.bold,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.black87)),
                  onPressed: () {
                    onTapFoodPage(context);
                  },
                  child: Text(
                    'Types of Food',
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                  color: Colors.blueAccent,
                ),
                RaisedButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.black87)),
                  onPressed: () {
                    onTapResPage(context);
                  },
                  child: Text('List of Restaurants',
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
                  color: Colors.blueAccent,
                ),
              ],
            ),
          ]),
        ));
  }
}
