import 'package:flutter/material.dart';
import '../images_banner.dart';
import '../infoTile.dart';
import '../models/food.dart';
import '../detail/res_screenDisplay.dart';
import '../controller.dart';

class foodPage extends StatelessWidget {
  final String _foodType;
  foodPage(this._foodType);
  @override
  Widget build(BuildContext context) {
    final food = Food.fetchByID(_foodType);

    return Scaffold(
        appBar: AppBar(
          title: Text(food.name),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.favorite),
              onPressed: () {
                onTapFav(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.person_add),
              onPressed: () {
                onTapFriend(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                onTapSettings(context);
              },
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(children: [
              ImageBanner(assetPath: food.imgPath),
              DisplayFoodTile(food),
              Container(
                child: RaisedButton(
                  //Open Google Maps using res addr
                  onPressed: () {},
                  child: Text('Go Here!',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  color: Colors.blueAccent,
                ),
              ),
            ]
                //]..addAll(textSections(food)),
                ),
          ),
        ));
  }

  // map = List of one type converted into a list of a differnt type
  //toList makes something iterable into a list
  //List<Widget> textSections(Food food) {
  //  return food.info.map((info) => Res_detail(info.title, //info.text)).toList();
  //}
}
