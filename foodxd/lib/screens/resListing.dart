import 'package:flutter/material.dart';
import '../models/restaurant.dart';
import '../images_banner.dart';
import '../infoTile.dart';
import 'package:fooxd/controller.dart';

class ResListing extends StatefulWidget {
  ResListing1 createState() => new ResListing1();
}

class ResListing1 extends State<ResListing> {
  @override
  Widget build(BuildContext context) {
    final restaurants = Restaurant.fetchAll();
    return Scaffold(
        appBar: AppBar(
          title: Text("Restaurant Listing"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.favorite),
              onPressed: () {
                onTapFav(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.person_add),
              onPressed: () {
                onTapFriend(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                onTapSettings(context);
              },
            ),
          ],
        ),
        body: ListView.builder(
          itemCount: restaurants.length,
          itemBuilder: (context, index) =>
              _itemBuilder(context, restaurants[index]),
        ));
  }

  Widget _itemBuilder(BuildContext context, Restaurant res) {
    return Stack(
      children: [
        Container(
          child: GestureDetector(
            child: ImageBanner(assetPath: res.imgPath, height: 250.0),
            onTap: () => onLocationTap(context, res.id),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(0, 160, 0, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(child: DisplayTile(res), onDoubleTap: () => {}),
            ],
          ),
        ),
      ],
    );
  }
}
