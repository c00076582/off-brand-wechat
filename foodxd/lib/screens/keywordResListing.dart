import 'package:flutter/material.dart';
import '../models/restaurant.dart';
import '../images_banner.dart';
import '../infoTile.dart';
import 'package:fooxd/controller.dart';

class KeywordResListing extends StatelessWidget {
  final String keyword;
  KeywordResListing(this.keyword);
  List<Restaurant> restaurant = [];

  @override
  Widget build(BuildContext context) {
    final res = Restaurant.fetchAll();
    for (int i = 0; i < res.length; i++) {
      if (res[i].keyword == keyword) {
        restaurant.add(res[i]);
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: Text("Keyword Listing"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.favorite),
              onPressed: () {
                onTapFav(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.person_add),
              onPressed: () {
                onTapFriend(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                onTapSettings(context);
              },
            ),
          ],
        ),
        body: ListView.builder(
            itemCount: restaurant.length,
            itemBuilder: (context, index) =>
                _itemBuilder(context, restaurant[index])));
  }

  Widget _itemBuilder(BuildContext context, Restaurant res) {
    return GestureDetector(
      child: Container(
        height: 250.0,
        child: Stack(
          children: [
            ImageBanner(assetPath: res.imgPath, height: 250.0),
            DisplayTile(res)
          ],
        ),
      ),
      onTap: () => onLocationTap(context, res.id),
    );
  }
}
