import 'package:flutter/material.dart';
import '../images_banner.dart';
import '../detail/res_screenDisplay.dart';
import '../models/restaurant.dart';
import '../infoTile.dart';
import 'package:fooxd/controller.dart';

class RestaurantPage extends StatelessWidget {
  final int _resID;
  RestaurantPage(this._resID);

  @override
  Widget build(BuildContext context) {
    final restaurant = Restaurant.fetchByID(_resID);
    return Scaffold(
        appBar: AppBar(
          title: Text(restaurant.name),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.favorite),
              onPressed: () {
                onTapFav(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.person_add),
              onPressed: () {
                onTapFriend(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                onTapSettings(context);
              },
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                ImageBanner(assetPath: restaurant.imgPath),
                DisplayTile(restaurant),
                Container(
                  child: RaisedButton(
                    //Open Google Maps using res addr
                    onPressed: () {
                      onTapGoogleMaps(context);
                    },
                    child: Text('Go Here!',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        )),
                    color: Colors.blueAccent,
                  ),
                ),
              ]..addAll(textSections(restaurant)),
            ),
          ),
        ));
  }

  // map = List of one type converted into a list of a differnt type
  //toList makes something iterable into a list
  List<Widget> textSections(Restaurant restaurant) {
    return restaurant.info
        .map((info) => Res_detail(info.title, info.text))
        .toList();
  }
}
