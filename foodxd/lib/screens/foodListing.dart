import 'package:flutter/material.dart';
import '../models/food.dart';
import '../images_banner.dart';
import '../infoTile.dart';
import 'package:fooxd/controller.dart';

class FoodListing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final food = Food.fetchAll();
    return Scaffold(
        appBar: AppBar(
          title: Text("Food Listing"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.favorite),
              onPressed: () {
                onTapFav(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.person_add),
              onPressed: () {
                onTapFriend(context);
              },
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                onTapSettings(context);
              },
            ),
          ],
        ),
        body: ListView.builder(
          itemCount: food.length,
          itemBuilder: (context, index) => _itemBuilder(context, food[index]),
        ));
  }

  Widget _itemBuilder(BuildContext context, Food food) {
    return GestureDetector(
      child: Container(
        height: 250.0,
        child: Stack(
          children: [
            ImageBanner(assetPath: food.imgPath, height: 250.0),
            DisplayFoodTile(food)
          ],
        ),
      ),
      onTap: () => onFoodLocationTap(context, food.name),
    );
  }
}
