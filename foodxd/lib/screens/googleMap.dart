import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  GoogleMapController mapController;
  final Set<Marker> _markers = {};
  static LatLng _center = const LatLng(30.2241, -92.0198);

  LatLng lastMapPosition = _center;

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Map Sample'),
          backgroundColor: Colors.blue,
        ),
        body: Stack(
          children: [
            GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 11.0,
              ),
              markers: _markers,
              onCameraMove: onCameraMove, //delete
            ),
            //delete button
            FloatingActionButton(
              onPressed: onTapAddMarker,
              child: Icon(Icons.add_location),
            ),
          ],
        ),
      ),
    );
  }

  //testing purpose, replace with res loc
  void onCameraMove(CameraPosition position) {
    lastMapPosition = position.target;
  }

  void onTapAddMarker() {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(lastMapPosition.toString()),
          position: lastMapPosition,
          infoWindow: InfoWindow(
            title: "Hello",
            snippet: "Snippet of marker",
          )));
    });
  }
}
