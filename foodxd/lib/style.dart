import 'package:flutter/material.dart';

const LargeSize = 26.0;
const MediumSize = 20.0;
const SmallSize = 18.0;

const TitleStyle = TextStyle(
  fontWeight: FontWeight.w300,
  fontSize: LargeSize,
  color: Colors.black,
);
const Body1Style = TextStyle(
  fontWeight: FontWeight.w300,
  fontSize: SmallSize,
  color: Colors.black,
);
