import 'package:flutter/material.dart';
import 'screens/index.dart';
import 'style.dart';
import 'screens/restaurantPage.dart';
import 'screens/foodPage.dart';
import 'screens/keywordResListing.dart';

void main() => runApp(App());

const IndexRoute = '/';
const ResPage = '/restaurantPage';
const FoodPage = '/foodPage';
const KeywordResPage = '/keywordRestaurantPage';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        //home: IndexPage(),
        onGenerateRoute: _routes(),
        theme: ThemeData(
            textTheme: TextTheme(
          title: TitleStyle,
          body1: Body1Style,
        )));
  }

  RouteFactory _routes() {
    return (settings) {
      final Map<String, dynamic> arguments = settings.arguments;
      Widget screen;
      switch (settings.name) {
        case IndexRoute:
          screen = IndexPage();
          break;
        case ResPage:
          screen = RestaurantPage(arguments['id']);
          break;
        case KeywordResPage:
          screen = KeywordResListing(arguments['keyword']);
          break;
        default:
          return null;
      }
      return MaterialPageRoute(builder: (BuildContext context) => screen);
    };
  }
}
