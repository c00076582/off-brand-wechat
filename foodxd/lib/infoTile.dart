import 'package:flutter/material.dart';
import 'models/restaurant.dart';
import 'models/food.dart';

class DisplayTile extends StatefulWidget {
  final Restaurant res;
  DisplayTile(this.res);
  DisplayTile1 createState() => new DisplayTile1(res);
}

class DisplayTile1 extends State<DisplayTile> {
  final Restaurant res;
  final Set<Restaurant> savedRestaurant = new Set<Restaurant>();
  DisplayTile1(this.res);

  @override
  Widget build(BuildContext context) {
    final bool alreadySaved = savedRestaurant.contains(res);
    return Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 5.0),
            decoration: BoxDecoration(color: Colors.black.withOpacity(.77)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Row(
                    children: [
                      InfoTile(res),
                    ],
                  ),
                ),
                Container(
                  child: GestureDetector(
                    child: Icon(
                        alreadySaved ? Icons.favorite : Icons.favorite_border,
                        color: alreadySaved ? Colors.red : Colors.white,
                        size: 42),
                    onTap: () => {
                      setState(() {
                        if (alreadySaved) {
                          savedRestaurant.remove(res);
                        } else {
                          savedRestaurant.add(res);
                        }
                      })
                    },
                  ),
                )
              ],
            ),
          )
        ]);
  }
}

class InfoTile extends StatelessWidget {
  final Restaurant res;
  InfoTile(this.res);
  @override
  Widget build(BuildContext context) {
    final textColor = Colors.white;
    const bigSize = 24.0;
    const smallSize = 16.0;
    final textColor1 = Colors.redAccent;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      height: 80,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              res.name.toUpperCase(),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                  color: textColor1,
                  fontWeight: FontWeight.w400,
                  fontSize: bigSize),
            ),
            Text("Rating: " + res.review + "/5",
                maxLines: 1,
                style: TextStyle(
                    color: textColor,
                    fontSize: smallSize,
                    fontWeight: FontWeight.w400)),
            Text("Distance: XX Miles Away",
                maxLines: 1,
                style: TextStyle(
                    color: textColor,
                    fontWeight: FontWeight.w400,
                    fontSize: smallSize)),
          ]),
    );
  }
}

class DisplayFoodTile extends StatelessWidget {
  final Food food;

  DisplayFoodTile(this.food);
  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
              padding: EdgeInsets.symmetric(vertical: 5.0),
              decoration: BoxDecoration(color: Colors.black.withOpacity(.66)),
              child: InfoFoodTile(food))
        ]);
  }
}

class InfoFoodTile extends StatelessWidget {
  final Food food;
  InfoFoodTile(this.food);
  @override
  Widget build(BuildContext context) {
    const bigSize = 32.0;
    const color = const Color(0xffdedede);
    final textColor1 = color;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      height: 80,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              food.name.toUpperCase(),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                  color: textColor1,
                  fontWeight: FontWeight.w400,
                  fontSize: bigSize),
            ),
          ]),
    );
  }
}
