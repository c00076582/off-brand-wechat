import pytest
from verificationTest import login_validate, password_validate

def test_login():
    login_validate('user')
    login_validate('')

def test_password():
    password_validate('pass')
    password_validate('')
    password_validate(1)
